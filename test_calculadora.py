import unittest
from calculadora import Calculadora

class TestCalculadora(unittest.TestCase):
    def test_uno_mas_uno(self):
        calc = Calculadora()
        resultado = calc.sumar(1, 1)
        self.assertEqual(resultado, 2)

if __name__ == '__main__':
    unittest.main()